//
//  Player.swift
//  p07
//
//  Created by Shivani Awate on 4/30/17.
//  Copyright © 2017 shivani. All rights reserved.
//

import SpriteKit

class Player: SKSpriteNode{
    
    private var minX = CGFloat(-200), maxX = CGFloat(200);
    
    func initializePlayer() {
      name = "Player";
        
        physicsBody = SKPhysicsBody (circleOfRadius: size.height / 2  );
        physicsBody?.affectedByGravity = false;
        physicsBody?.isDynamic = false;
        physicsBody?.categoryBitMask = ColliderType.PLAYER;
        physicsBody?.contactTestBitMask = ColliderType.Fruit_And_Bomb;
        
        
    
    }
    
    func move(left: Bool) {
        if left{
            
            position.x -= 15;
            
            if position.x < minX {
                position.x = minX;
            }
            
        } else {
            
            position.x += 15;
            
            if position.x > maxX {
                position.x = maxX;  //maximum x position which will not allow the player to go maximum
            }
        
        }
    }
    
} //class
