//
//  MainMenuScene.swift
//  p07
//
//  Created by Shivani Awate on 4/29/17.
//  Copyright © 2017 shivani. All rights reserved.
//

import SpriteKit

class MainMenuScene: SKScene {
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        for touch in touches{
            let location = touch.location(in: self); //get the location of that touch
            
            if atPoint(location).name == "Start" {   //returns a node
                if let scene = GameplaySceneClass(fileNamed: "GameplayScene") {
                    // Set the scale mode to scale to fit the window
                    scene.scaleMode = .aspectFill
                    
                    // Present the scene
                    
                    view!.presentScene(scene, transition: SKTransition.doorsOpenVertical(withDuration:TimeInterval(2)));
                    
                }
                
            }
            
        }
    }

}
