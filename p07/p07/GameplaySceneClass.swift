//
//  GameplaySceneClass.swift
//  p07
//
//  Created by Shivani Awate on 4/29/17.
//  Copyright © 2017 shivani. All rights reserved.
//

import SpriteKit

class GameplaySceneClass: SKScene , SKPhysicsContactDelegate {
    
    private var player: Player?;
    
    private var center = CGFloat();
    
    private var canMove = false, moveLeft = false;
    
    private var itemController = ItemController();
    
    private var scoreLabel: SKLabelNode?
    
    private var score = 0;
    
    override func didMove(to view: SKView) {
        initializeGame();
        
    }
    
    override func update(_ currentTime: TimeInterval) {
        managePlayer();
    }
    
    
    
    //to move our player
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        for touch in touches {
            let location = touch.location(in: self); //get location of the touch
            
            if location.x > center {
                moveLeft = false; //that is we are moving to the right side
            } else {
                moveLeft = true;
            }
        }
        canMove = true;
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        canMove = false;
    }
    
    
    func didBegin(_ contact: SKPhysicsContact) {

        var firstBody = SKPhysicsBody();
        var secondBody = SKPhysicsBody();
        
        if contact.bodyA.node?.name == "Player" {
        
            firstBody = contact.bodyA;
            secondBody = contact.bodyB;
        }else
        {
            firstBody = contact.bodyB;
            secondBody = contact.bodyA;
        
        }
        
        if firstBody.node?.name == "Player" && secondBody .node?.name == "Fruit1" {
            score += 1;
            scoreLabel?.text = String(score);
            secondBody.node?.removeFromParent();
            
            
            
            
        
        
        }
        
        if firstBody.node?.name == "Player" && secondBody .node?.name == "bomb" {
            
            firstBody.node?.removeFromParent();
            secondBody.node?.removeFromParent();
            
            Timer.scheduledTimer(timeInterval: TimeInterval(2), target: self, selector: #selector(GameplaySceneClass.restartGame), userInfo: nil, repeats: false);
            
        }
        
    }
    
    private func initializeGame() {
        
        physicsWorld.contactDelegate = self;
        
        player = childNode(withName: "Player") as? Player!; //getting the child(player)...getting it as a player class
        
        player?.initializePlayer();
        scoreLabel = childNode(withName: "ScoreLabel") as? SKLabelNode;
        scoreLabel?.text = "0";
        
        
        //calculate the center
        center = self.frame.size.width / self.frame.size.height;
        
        Timer.scheduledTimer(timeInterval: TimeInterval(itemController.randomBetweenNumber(firstNum: 1, secondNum: 2)), target: self, selector: #selector(GameplaySceneClass.spawnItems), userInfo: nil, repeats: true);
        
        Timer.scheduledTimer(timeInterval: TimeInterval(7), target: self, selector: #selector(GameplaySceneClass.removeItems), userInfo: nil, repeats: true);
        
        
        
    }

    
    
    private func managePlayer() {
        if canMove {
            player?.move(left: moveLeft);
        }
   }
    
    func spawnItems(){
    
         self.scene?.addChild(itemController.spawnItems());

    }
    
    
    func restartGame(){
    
        if let scene = GameplaySceneClass(fileNamed: "GamePlayScene") {
        
        scene.scaleMode = .aspectFill
            view?.presentScene(scene, transition: SKTransition.doorsOpenHorizontal(withDuration: TimeInterval(2)))
        
        }
        
    }
    
    func removeItems() {
        for child in children {
            if child.name == "Fruit1" || child.name == "bomb" {
                if child.position.y < -self.scene!.frame.height - 100 {
                    child.removeFromParent();
                }
            }
        }
    }
    
    
}
