//
//  ItemController.swift
//  p07
//
//  Created by urvashi khatuja on 5/1/17.
//  Copyright © 2017 shivani. All rights reserved.
//

import SpriteKit

struct ColliderType {

    static let PLAYER: UInt32 = 0;
    static let Fruit_And_Bomb:  UInt32 = 1;

}

class ItemController {
    
    private var minX = CGFloat(-200), maxX = CGFloat(200);

    func spawnItems() -> SKSpriteNode {
        
        let item : SKSpriteNode?;
        
        if Int (randomBetweenNumber(firstNum: 0, secondNum: 10)) >= 6 {
        
            item = SKSpriteNode (imageNamed: "bomb");
            item!.name = "bomb";
            item!.setScale(0.6);
            item!.physicsBody = SKPhysicsBody (circleOfRadius: item!.size.height / 2);
        } else
        
        {
            let num = Int (randomBetweenNumber(firstNum: 1, secondNum: 5));
            item = SKSpriteNode (imageNamed: "Fruit1");
            
            //item = SKSpriteNode (imageNamed: "Fruit \(num)" );
            item!.name = "Fruit1";
            item!.setScale(0.7);
            item!.physicsBody = SKPhysicsBody(circleOfRadius: item!.size.height / 2 );
        }
    
        item!.physicsBody?.categoryBitMask = ColliderType.Fruit_And_Bomb;
        
        item!.zPosition = 3;
        item!.anchorPoint = CGPoint (x: 0.5, y: 0.5);
        item!.position.x = randomBetweenNumber(firstNum: minX,
                                               secondNum: maxX);
        item!.position.y = 500;
        
        
        
        return item!;
        
    }

    func randomBetweenNumber(firstNum: CGFloat, secondNum: CGFloat)
     -> CGFloat {
    
        return CGFloat(arc4random()) / CGFloat (UINT32_MAX) * abs (firstNum - secondNum) + min (firstNum,secondNum);
    }
}
